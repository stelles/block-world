﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace BlocksWorlds
{
    /// <summary>
    /// Interaction logic for UserInterface.xaml
    /// </summary>
    public partial class MainWindow : UserControl
    {

        private Blocksworld _world;
        private StringWriter _writer;
        private StringBuilder _test;

        public MainWindow()
        {
            InitializeComponent();

            this._test = new StringBuilder();
            this._writer = new StringWriter(this._test);
            Console.SetOut(this._writer);
            Console.SetError(this._writer);

            this._world = new Blocksworld();
        }


        private void Quit(object sender, MouseButtonEventArgs e)
        {
            
        }

        private void CommandLine_Enter(object sender, KeyEventArgs e)
        {
            if (e.Key != System.Windows.Input.Key.Enter) return;

            if (CommandLine.Text != "")
            {
                this._world.ProcessLine(CommandLine.Text);
                this._writer.Flush();
                Output.Text = "> " + this._test + "\n" + Output.Text;
                this._test.Clear();
                CommandLine.Text = "";
            }
        }

        private void MenuItem_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            return;
        }
    }
}
