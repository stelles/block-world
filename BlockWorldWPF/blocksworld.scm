(define x '(
    (ontable "1")(on "2" "1")(clear "2")
    (ontable "3")(on "4" "3")(on "5" "4")(clear "5")
    (armempty)
  )
)

;;; Utility Functions		

(define (pred p w)
  (find
    (lambda (n)
      (eq? (car n) p)
    )
  w)
)

(define (pred1 p a w)
  (find
    (lambda (n)
      (and (eq? (car n) p) (eq? (cadr n) a))
    )
  w)
)

(define (pred2 p a b w)
  (find
    (lambda (n)
      (and (and (eq? (car n) p) (eq? (cadr n) a))(eq? (caddr n) b))
    )
  w)
)

(define (pred-2 p b w)
  (find
    (lambda (n)
      (and (eq? (car n) p) (eq? (caddr n) b))
    )
  w)
)

(define (remove x list)
  (if (null? list)
    '()
    (if (equal? (car list) x)
      (cdr list)
      (cons (car list) (remove x (cdr list)))
    )
  )
)
;;; Predicates

(define (on-which? a w)
  (let ([x (pred1 'on a w)])
    (if (list? x)
      (cddr x)
      '()
    )
  )
)

(define (on a b w)
  (list? (pred2 'on a b w))
)

(define (clear a w)
  (list? (pred1 'clear a w))
)

(define (ontable a w)
  (list? (pred1 'ontable a w))
)

(define (holding a w)
  (list? (pred1 'holding a w))
)

(define (armempty w)
  (list? (pred 'armempty w))
)

;;; Operators

(define (stack a b w)
  (if (and (and (clear a w) (clear b w)) (armempty w))
    (let* ([r (on-which? a w)][x (if (null? r) '() (car r))])
      (append
	(if (null? r)
	  '()
	  (list (list 'clear x))
	)
      (cons (list 'on a b) (remove (list 'clear b) (remove (list 'ontable a) (remove (list 'on a x) w))))
      )
    )
    (if (and (holding a w) (clear b w))
      (cons '(armempty) (cons (list 'clear a) (cons (list 'on a b) (remove (list 'clear b) (remove (list 'holding a) w)))))
      '()
    )
  )
)

(define (unstack a b w)
  (if (and (and (clear a w) (on a b w)) (armempty w))
    (cons (list 'clear b) (cons (list 'ontable a) (remove (list 'on a b) w)))
    '()
  )
)

(define (pickup a w)
  (if (and (clear a w) (armempty w))
    (let* ([r (on-which? a w)][x (if (null? r) '() (car r))])
      (append
        (if (null? r) '() (list (list 'clear x)))
	(cons (list 'holding a) (remove '(armempty) (remove (list 'on a x) (remove (list 'clear a) (remove (list 'ontable a) w)))))
      )
    )
    '()
  )
)

(define (putdown a w)
  (if (holding a w)
    (cons (list 'ontable a) (cons (list 'clear a) (cons '(armempty) (remove (list 'holding a) w))))
    '()
  )
)

(define (addblock a w)
  (if (list? (find (lambda (n) (not (eq? #f n))) (map (lambda (n) (memq a n)) w)))
    '()
    (cons (list 'ontable a) (cons (list 'clear a) w))
  )
)

(define (removeblock a w)
  (if (and (clear a w) (armempty w))
    (let* ([x (on-which? a w)][o (if (null? x) '() (list 'on a (car x)))])
      (append
        (if (null? x) '() (list (list 'clear (car x))))
        (remove (list 'ontable a) (remove (list 'clear a) (remove o w)))
      )
    )
  )
)

(define (initialize w);Currently does not make sure that the system is fully defined. Not sure if this is necessary
  (if (and (not (symbol? (find (lambda (n) (not (list? n))) w))) 
           (or (list? (pred 'holding w)) (list? (pred 'armempty w))))
    (initialize-tail w '())
    '()
  )
)

(define (initialize-tail f b);Might only need to look at the tail for each itteration since if the front few are already compatible they don't need to be rechecked
  (if (null? f)
    (append f b)
    (let* ([w (append (cdr f) b)][i (car f)])
      (if (valid? i w)
        (initialize-tail (cdr f) (cons i b))
	'()
      )
    )
  )
)

(define (nl l)
  (not (list? l))
)

(define (valid? p w)
  (if (null? p)
    #f
    (let* ([len (length p)]
	   [predicate (car p)]
	   [a (if (>= len 2) (cadr p) '())]
	   [b (if (equal? len 3) (caddr p) '())])
      (cond
        [(eq? predicate 'on) (and (equal? len 3)
                             (and (nl(pred1 'holding a w)) (and (nl(pred1 'holding b w)) (and (nl(pred1 'ontable a w))(and (nl(pred1 'clear b w))(and (nl(pred-2 'on b w))(nl(pred1 'on a w))))))))]
	[(eq? predicate 'ontable) (and (equal? len 2)
                                  (and (nl(pred1 'ontable a w))(nl(pred1 'on a w))))]
	[(eq? predicate 'clear) (and (equal? len 2)
				(and (nl(pred1 'holding a w))
                                (and (nl (pred-2 'on a w)) (nl(pred1 'clear a w)))))]
	[(eq? predicate 'holding) (and (equal? len 2)
				  (and (and (and (and (nl (pred1 'on a w)) (nl (pred-2 'on a w))) (nl (pred1 'ontable a w))) (nl (pred1 'clear a w)))
                                  (and (nl (pred 'holding w)) (nl (pred 'armempty w)))))]
	[(eq? predicate 'armempty) (and (equal? len 1)(and (not (list? (pred 'holding w))) (not (list? (pred 'armempty w)))))]
	[else #f]
      )
    )
  )
)
#|
initialize

Using (primitive-eval exp) might be usefule when initializing
|#
