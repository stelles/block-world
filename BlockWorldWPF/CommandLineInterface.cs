﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace BlocksWorlds
{
    class CommandLineInterface
    {
        private Blocksworld _world;

        public CommandLineInterface()
        {
            this._world = new Blocksworld();
        }

        public void Loop()
        {
            string line = Console.ReadLine();
            while (line != null && line != "")
            {
                this._world.ProcessLine(line);
                line = Console.ReadLine();
            }
        }
    }
}
