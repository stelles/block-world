﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IronScheme;
using NLua;

namespace BlocksWorlds
{
    public class Blocksworld
    {
        private Lua _luaState;

        public class Block
        {
            private string _name;
            private Block _top;
            private Block _bottom;

            public Block(string name)
            {
                _name = name;
            }

            public string Name
            {
                get { return _name; }
            }

            public Block Top
            {
                get { return _top;  }
                set { _top = value;  }
            }

            public Block Bottom
            {
                get { return _bottom; }
                set { _bottom = value; }
            }
        }

        public class WorldState
        {
            public Block Hand;
            private IronScheme.Runtime.Cons _state;
            private List<Block> _blocks;

            public WorldState(IronScheme.Runtime.Cons state, String[] blocks)
            {
                this._state = state;
                this._blocks = new List<Block>();
                foreach (String s in blocks)
                {
                    this._blocks.Add(new Block(s));
                }
            }

            public WorldState(IronScheme.Runtime.Cons state, List<Block> blocks)
            {
                this._state = state;
                this._blocks = blocks;
            }

            public IronScheme.Runtime.Cons State
            {
                get { return this._state; }
            }

            public List<Block> Blocks
            {
                get { return this._blocks; }
            }

            public override string ToString()
            {
                return (string)"(format \"~a\" {0})".Eval(this._state);
            }

            public void PrintBlocks()
            {
                foreach (Block b in this._blocks) {
                    Console.Write(b.Name + ", ");
                }
                Console.WriteLine();
            }
        }

        public Blocksworld()
        {
            var fileContents = File.ReadAllText("blocksworld.scm");
            this.Evaluate(fileContents);

            this._luaState = new Lua();
            this._luaState["World"] = this;
            this._luaState.RegisterFunction("print", typeof(Blocksworld).GetMethod("print"));
        }

        public static void print(object text) {
            Console.Write(text.ToString());
        }

        public void Display(WorldState state)
        {
            if (state.Hand != null)
            {
                Console.WriteLine("Holding: " + state.Hand.Name);
            }
            foreach (Block block in state.Blocks)
            {
                if (block.Bottom == null)
                {
                    Block theBlock = block;
                    while (theBlock != null)
                    {
                        Console.Write(theBlock.Name + " -> ");
                        theBlock = theBlock.Top;
                    }
                    Console.WriteLine();
                }
            }
        }

        private object Evaluate(string input)
        {
            try
            {
                return input.Eval(); // calls IronScheme.RuntimeExtensions.Eval(string)
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return "";
            }
        }

        //TODO all modification functions currently mess up previous states. Need to deep copy blocks before modification
        public WorldState Stack(Block blockA, Block blockB, WorldState state)
        {
            try
            {
                var result = "(stack {0} {1} {2})".Eval(blockA.Name, blockB.Name, state.State);
                if (result == null)
                {
                    return null;
                }
                else
                {
                    if (blockA.Bottom != null)
                    {
                        blockA.Bottom.Top = null;
                    }
                    blockA.Bottom = blockB;
                    blockB.Top = blockA;
                    return new WorldState((IronScheme.Runtime.Cons)result, state.Blocks);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return null;
            }
        }

        public WorldState Unstack(Block blockA, Block blockB, WorldState state)
        {
            try
            {
                var result = "(unstack {0} {1} {2})".Eval(blockA.Name, blockB.Name, state.State);
                if ((bool)"(null? {0})".Eval(result))
                {
                    return null;
                }
                else
                {
                    blockA.Bottom = null;
                    blockB.Top = null;
                    return new WorldState((IronScheme.Runtime.Cons)result, state.Blocks);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return null;
            }
        }

        public WorldState Pickup(Block block, WorldState state)
        {
            try
            {
                var result = "(pickup {0} {1})".Eval(block.Name, state.State);
                if ((bool)"(null? {0})".Eval(result))
                {
                    return null;
                }
                else
                {
                    if (block.Bottom != null)
                    {
                        block.Bottom.Top = null;
                    }

                    block.Bottom = block;
                    WorldState newState = new WorldState((IronScheme.Runtime.Cons)result, state.Blocks);
                    newState.Hand = block;

                    return newState;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return null;
            }
        }

        public WorldState Putdown(Block block, WorldState state)
        {
            try
            {
                var result = "(putdown {0} {1})".Eval(block.Name, state.State);
                if ((bool)"(null? {0})".Eval(result))
                {
                    return null;
                }
                else
                {
                    state.Hand.Bottom = null;
                    return new WorldState((IronScheme.Runtime.Cons)result, state.Blocks);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return null;
            }
        }

        public WorldState AddBlock(string name, WorldState state)
        {
            try
            {
                var result = "(addblock {0} {1})".Eval(name, state.State);
                if (result == null)
                {
                    return null;
                }
                else
                {
                    List<Block> newBlocks = new List<Block>();
                    foreach (Block b in state.Blocks)
                    {
                        newBlocks.Add(b);
                    }
                    newBlocks.Add(new Block(name));
                    return new WorldState((IronScheme.Runtime.Cons)result, newBlocks);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return null;
            }
        }

        public WorldState RemoveBlock(Block block, WorldState state)
        {
            try
            {
                var result = "(removeblock {0} {1})".Eval(block.Name, state.State);
                if (result == null)
                {
                    return null;
                }
                else
                {
                    if (block.Bottom != null)
                    {
                        block.Bottom.Top = null;
                    }
                    List<Block> newBlocks = new List<Block>();
                    foreach (Block b in state.Blocks)
                    {
                        newBlocks.Add(b);
                    }
                    newBlocks.Remove(block);
                    return new WorldState((IronScheme.Runtime.Cons)result, newBlocks);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return null;
            }
        }

        public bool OnTable(Block block, WorldState state)
        {
            try
            {
                return (Boolean)"(ontable {0} {1})".Eval(block.Name, state.State);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return false;
            }
        }

        public bool On(Block blockA, Block blockB, WorldState state)
        {
            try
            {
                return (Boolean)"(on {0} {1} {2})".Eval(blockA.Name, blockB.Name, state.State);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return false;
            }
        }

        public bool Holding(Block block, WorldState state)
        {
            try
            {
                return (Boolean)"(holding {0} {1})".Eval(block.Name, state.State);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return false;
            }
        }

        public bool ArmEmpty(WorldState state)
        {
            try
            {
                return (Boolean)"(armempty {0})".Eval(state.State);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return false;
            }
        }

        public bool ProcessLine(String input)
        {
            try
            {
                this._luaState.DoString(input);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return false;
            }
            return true;
        }

        public WorldState EmptyWorld()
        {
            IronScheme.Runtime.Cons state = (IronScheme.Runtime.Cons)this.Evaluate("(initialize '((armempty)))");
            if (state == null) {
                return null;
            }
            return new WorldState(state, new List<Block>());
        }

        public void SaveWorld(String outputFilename, WorldState state)
        {
            string blocks = "";
            foreach (Block b in state.Blocks)
            {
                if (b.Bottom == null)
                {
                    blocks = blocks + b.Name;
                    Block theBlock = b.Top;
                    while (theBlock != null)
                    {
                        blocks = blocks + "," + theBlock.Name;
                        theBlock = theBlock.Top;
                    }
                    blocks = blocks + ";";
                }
            }
            var text = "(format \"~a\" {0})".Eval(state.State);
            File.WriteAllText(outputFilename, blocks + "\n" + (String)text);
        }

        public WorldState LoadWorld(String inputFilename)
        {
            char[] lineDelim = { '\n' };
            char[] stackDelim = { ';' };
            char[] blockDelim = { ',' };
            string text = File.ReadAllText(inputFilename);
            string[] lines = text.Split(lineDelim);
            if (lines.Length != 2)
            {
                Console.WriteLine("Unable to load world: invalid world");
                return null;
            }
            try
            {
                String command = "(initialize '" + lines[1] + ")";
                var result = command.Eval();
                if (result == null)
                {
                    Console.WriteLine("Unable to load world: invalid world");
                    return null;
                }
                else
                {
                    List<Block> newBlocks = new List<Block>();
                    foreach (string s in lines[0].Split(blockDelim)) {
                        newBlocks.Add(new Block(s));
                    }
                    return new WorldState((IronScheme.Runtime.Cons)result, newBlocks);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return null;
            }
        }
    }
}
